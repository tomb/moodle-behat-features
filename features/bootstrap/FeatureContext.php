<?php

use Behat\Behat\Context\BehatContext,
    Sanpi\Behatch\Context\BehatchContext,
    Moodle\Behat\Context\MoodleContext;

/**
 * Features class
 *
 * It adds the behatch contexts (REST, JSON... steps definitions) and
 * Moodle contexts (common steps definitions + components steps definitions)
 *
 * @link http://packagist.org/packages/moodlehq/behat-contexts} For more info
 * @copyright 2012 David Monllaó
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class FeatureContext extends BehatContext
{
    /** @var array The application settings */
    private $parameters;

    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;

        // Assigning subcontexts of the main context
        $this->useContext('behatch', new BehatchContext($parameters));
        $this->useContext('moodle', new MoodleContext($parameters));
    }
}

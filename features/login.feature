# features/login.feature
Feature: Login
  In order to login
  As a moodle user
  I need to be able to validate the username and password against moodle

  @mink:selenium2
  Scenario: Login as an existing user
    Given I am on "login/index.php"
    When I fill in "username" with "admin"
    And I fill in "password" with "moodle"
    And I press "loginbtn"
    Then I should see "You are logged in as"

  @mink:selenium2
  Scenario: Login as an unexisting user
    Given I am on "login/index.php"
    When I fill in "username" with "adminasdasd"
    And I fill in "password" with "moodlesdfasdf"
    And I press "loginbtn"
    Then I should see "Invalid login, please try again"

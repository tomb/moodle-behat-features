Feature: Edit course params
  In order to edit course params
  As a moodle teacher
  I need o be able to change the course settings

  @mink:selenium2
  Scenario: Edit course params as a teacher
    Given I am logged as a "teacher"
    And I go to the settings page of a course
    And I fill in "fullname" with "AAAAAAAAAAAAAAAAA"
    When I press "id_submitbutton"
    Then I should see "AAAAAAAAAAAAAAAAA"

  @mink:selenium2
  Scenario: Edit course params as a student
    Given I am logged as a "student"
    When I go to the settings page of a course
    Then I should see "Sorry, but you do not currently have permissions to do that"

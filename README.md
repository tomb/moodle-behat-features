moodle-behat-features
=====================

Installation
------------
Based on http://docs.behat.org/cookbook/behat_and_mink.html#method-1-composer

* Install PHPUnit 
* Clone the repository or copy it's contents
* cd to the directory
* "curl http://getcomposer.org/installer | php"
* "php composer.phar install"
* "bin/behat -h" should show help info
* "bin/behat -dl" should list steps definitions including the moodle steps definitions
* Configure behat.yml with your own Moodle installation URL and test data
**  Most of the params are used for the steps with undefined articles "logged as a XXXX" or "in a course" for example)
* Run selenium Server
** Download latest version of Selenium server standalone (http://seleniumhq.org/download/) 
** Run it with "java -jar selenium-server-standalone-YOURFILEVERSION.jar"

Usage
-----
* cd to the directory
* "bin/behat features/NAMEOFTHEFEATURE.feature

Profiles
--------
* Profiles are defined in behat.yml
* To execute behat using a specific profile "bin/behat --profile PROFILENAME features/NAMEOFTHEFEATURE.feature"